import React from 'react'; 
import Statdisplay from '../Statdisplay/Statdisplay'
import StatListDisplay from '../StatListDisplay/StatListDisplay' 
import axios from 'axios';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import './Stats.css'


class Stats extends React.Component {
 
    constructor(props) {
        super(props);
        this.state = {
            tableauTopPages : [],  
            chartAvgReadTime : [],
            chartVisitsnumbers : [],
            chartUsersnumbers : [] ,
            numberVisits : 0,
            numberUniqueUsers : 0,
            avgReadTime : 0,
            topPagesByVisits : [],
            topUsersByVisits : [],
            topPagesByReadtime : [],
            topMedias:{
                topAudios : [],
                topVideos : []
            },
            topPosts : [],
            numberNewUsers :  0 ,
            numberReturningUsers : 0 ,
            numberLoyalUsers : 0 , 
            bounceRate : {
                topPages : [],
                bounceRate : 0
            } ,
            topAuthors : [],
            readingTimeSections : {
                a15 : 0 ,
                a30 : 0 ,
                a45 : 0 , 
                a60 : 0 ,
                a90 : 0
            } , 
            scrollPalier : {
                pages : [],
                users : {
                a25 : 0 ,
                a50 : 0 ,
                a75 : 0 , 
                a90 : 0 
            }
            },
            usersByCountry : [],
            usersByRef : [],
            usersByDevice : [], 
            classify_traffic : {
                organic : 0 ,
                social : 0 ,
                direct : 0,
                refIntern : 0,
                mail : 0 ,
                others : 0,
                total : 0
            }, 
            newUsersChart : [],
            returningChart : [], 
            chartAvgDepth : [] 
        } 
    }

    componentDidUpdate (prevProps, prevState) { 
        /*
        let startDate = this.props.startDate.getFullYear()+'-' + ( parseInt( this.props.startDate.getMonth() ) + 1 ) + '-'+this.props.startDate.getUTCDate()
        let endDate = this.props.endDate.getFullYear()+'-' + ( parseInt( this.props.endDate.getMonth() ) + 1 ) + '-'+this.props.endDate.getUTCDate()
        let urls = JSON.stringify(this.props.urls)
        let startPublishDate = this.props.startPublishDate.getFullYear()+'-' + ( parseInt( this.props.startPublishDate.getMonth() ) + 1 ) + '-'+this.props.startPublishDate.getUTCDate()
        let endPublishDate = this.props.endPublishDate.getFullYear()+'-' + ( parseInt( this.props.endPublishDate.getMonth() ) + 1 ) + '-'+this.props.endPublishDate.getUTCDate()
        
        let args = {
            url: urls,
            startDate : startDate ,
            endDate : endDate,
            endPublishDate : endPublishDate ,
            startPublishDate : startPublishDate,
            numberOfItems : this.props.numberofItems ,
            banInkyteam : this.props.banInkyteam
        } 
        if (    this.props.startDate !== prevProps.startDate || 
                this.props.endDate !== prevProps.endDate || 
                this.props.urls.length !== prevProps.urls.length ||
                this.props.endPublishDate !== prevProps.endPublishDate || 
                this.props.startPublishDate !== prevProps.startPublishDate   || 
                this.props.banInkyteam !== prevProps.banInkyteam
            )   
        this.getData(args); 
        */
    }

    async getData() {
        let startDate = this.props.startDate.getFullYear()+'-' + ( parseInt( this.props.startDate.getMonth() ) + 1 ) + '-'+this.props.startDate.getUTCDate()
        let endDate = this.props.endDate.getFullYear()+'-' + ( parseInt( this.props.endDate.getMonth() ) + 1 ) + '-'+this.props.endDate.getUTCDate()
        let urls = JSON.stringify(this.props.urls)
        let startPublishDate = this.props.startPublishDate.getFullYear()+'-' + ( parseInt( this.props.startPublishDate.getMonth() ) + 1 ) + '-'+this.props.startPublishDate.getUTCDate()
        let endPublishDate = this.props.endPublishDate.getFullYear()+'-' + ( parseInt( this.props.endPublishDate.getMonth() ) + 1 ) + '-'+this.props.endPublishDate.getUTCDate()
        
        let args = {
            url: urls,
            startDate : startDate ,
            endDate : endDate,
            endPublishDate : endPublishDate ,
            startPublishDate : startPublishDate,
            numberOfItems : this.props.numberofItems ,
            banInkyteam : this.props.banInkyteam
        }  
        try {

            this.setState({isLoading:true})
            let responseStats = await axios.get( this.props.endPoint + 'stats/getstats' , { params: args })  
            let r1 = responseStats.data.chartVisitsAndUsers.map((e) => [e.date,e.users] )
            let r2 = responseStats.data.chartVisitsAndUsers.map((e) => [e.date,e.count] )
            let r3 = responseStats.data.chartAvgReadTime.map((e) => [e.date,e.count] )
            let r4 = responseStats.data.chartUsersStacked.newUsersChart.map((e) => [e.date,e.visitcount] )
            let r5 = responseStats.data.chartUsersStacked.returningChart.map((e) => [e.date,e.visitcount] )
            let r6 = responseStats.data.chartAvgDepth.map((e) => [e.date,e.scroll] )

            r1 = r1.sort((a,b) => b[0] - a[0]) 
            r2 = r2.sort((a,b) => b[0] - a[0]) 
            r3 = r3.sort((a,b) => b[0] - a[0])
            r4 = r4.sort((a,b) => b[0] - a[0])
            r5 = r5.sort((a,b) => b[0] - a[0])
            r6 = r6.sort((a,b) => b[0] - a[0])

            this.setState({ 
                chartUsersnumbers : r1 , 
                chartVisitsnumbers :r2 , 
                chartAvgReadTime : r3 , 
                newUsersChart : r4, 
                returningChart : r5,
                chartAvgDepth : r6
            }) 

            this.setState({
                numberVisits : responseStats.data.numberVisits , 
                numberUniqueUsers : responseStats.data.numberUniqueUsers ,
                avgReadTime : responseStats.data.avgReadTime ,
                topPagesByVisits : responseStats.data.topPagesByVisits ,
                topUsersByVisits : responseStats.data.topUsersByVisits ,
                topPagesByReadtime : responseStats.data.topPagesByReadtime ,
                topMedias : responseStats.data.topMedias ,
                numberNewUsers : responseStats.data.numberNewUsers ,
                numberReturningUsers : responseStats.data.numberReturningUsers ,
                numberLoyalUsers : responseStats.data.numberLoyalUsers ,
                //bounceRate : responseStats.data.bounceRate ,
                topPosts : responseStats.data.topPosts  ,
                topAuthors : responseStats.data.topAuthors , 
                readingTimeSections : responseStats.data.readingTimeSections,
                scrollPalier : responseStats.data.scrollPalier, 
                usersByCountry : responseStats.data.usersByCountry   ,
                usersByRef : responseStats.data.usersByRef    ,
                usersByDevice : responseStats.data.usersByDevice , 
                tableauTopPages : responseStats.data.tableauTopPages,
                classify_traffic : responseStats.data.classify_traffic, 
                isLoading : false 
            }) 
        
        } catch(e) {
            console.log(e)
        }  
    }

    render () {    

        const argsUniqueUsers = {
            title: {
                text: 'Daily Unique Users'
            }, 
            xAxis: {
                type: 'datetime', 
            }, 
            yAxis: {
                title: {
                    text: null
                }
            }, 
            tooltip: {
                crosshairs: true,
                shared: true, 
            },  
            series: [{
                        name: 'Number Of Users',
                        data: this.state.chartUsersnumbers,
                        zIndex: 1,
                        marker: {
                            fillColor: 'white',
                            lineWidth: 2,
                            lineColor: Highcharts.getOptions().colors[0]
                        }
            }]
        } 
        const argsViewPages = {
            title: {
                text: 'Daily View Pages'
            }, 
            xAxis: {
                type: 'datetime', 
            }, 
            yAxis: {
                title: {
                    text: null
                }
            }, 
            tooltip: {
                crosshairs: true,
                shared: true, 
            },  
            series: [{
                name: 'Number Of Views',
                data: this.state.chartVisitsnumbers,
                zIndex: 1,
                marker: {
                    fillColor: 'white',
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[0]
                }
            }]
        } 
        const argsAvgReadTime = {
            title: {
                text: 'Avg Visit Time'
            }, 
            xAxis: {
                type: 'datetime', 
            }, 
            yAxis: {
                title: {
                    text: null
                }
            }, 
            tooltip: {
                crosshairs: true,
                shared: true, 
            },  
            series: [{
            name: 'Avg Visit Time',
            data: this.state.chartAvgReadTime,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
            }]
        }		 
        const argsStackedUsers = {
            chart: {
              type: 'area'
            },
            title: {
              text: 'Number of Daily Users Stacked'
            },
            xAxis: {
                type: 'datetime', 
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            tooltip: {
              split: true,
              valueSuffix: ' User'
            },
            plotOptions: {
              area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
                }
              }
            },
            series: [{
                name: 'Number Of New Users',
                data: this.state.newUsersChart,
                zIndex: 1,
                marker: {
                    fillColor: 'white',
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[0]
                }
            },{
                name: 'Number Of Returning Users',
                data: this.state.returningChart,
                zIndex: 1,
                marker: {
                    fillColor: 'white',
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[0]
                }
            }]
        }
        const argsAvgDepth = {
            title: {
                text: 'Avg Depth Rate'
            }, 
            xAxis: {
                type: 'datetime', 
            }, 
            yAxis: {
                title: {
                    text: null
                }
            }, 
         
            series: [{
                name: 'Avg Depth Rate',
                data: this.state.chartAvgDepth,
                zIndex: 1,
                marker: {
                    fillColor: 'white',
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[0]
                }
            }]
        }
        
        return ( 
            <div> 
                { this.state.isLoading 
                ? <p>Is Loading </p> 
                : <div>
                    <button className="mb-5 submitbttn" onClick={ async () => { this.getData() }}>GET STATS</button>
                    <div>
                        <HighchartsReact highcharts={Highcharts}  options={argsStackedUsers} />
                        <HighchartsReact highcharts={Highcharts}  options={argsUniqueUsers} />
                        <HighchartsReact highcharts={Highcharts}  options={argsAvgDepth} />
                        <HighchartsReact highcharts={Highcharts}  options={argsViewPages} />
                        <HighchartsReact highcharts={Highcharts}  options={argsAvgReadTime} />
                    </div>
                   
                    <div className="table-container">
                        <table cellSpacing="0px">
                            <thead>
                                <tr>
                                    <td width="40%">Page</td>
                                    <td width="15%">Nbr Visits</td>
                                    <td width="15%">Saissons</td>
                                    <td width="15%">DepthRate</td>
                                    <td width="15%">avgReadTime</td>
                                </tr>
                            </thead>
                            <tbody> 
                                    {this.state.tableauTopPages.map((e) => { 
                                        return (
                                            <tr>
                                                <td>{e._id.url.substring(0,100)}</td>
                                                <td>{e.visitcount}</td>
                                                <td>{e.saissons}</td>
                                                <td>{e.depthRate.users.a50}%</td>
                                                <td>{e.avgReadTime/60} Minutes</td>
                                            </tr>
                                        ) 
                                    })}  
                            </tbody>
                        </table>
                    </div>
                    <div className="cardscontainer mt-5">
                        <Statdisplay statname="Number Of Visits" value={this.state.numberVisits}/>
                        <Statdisplay statname="Number Of Unique Users" value={this.state.numberUniqueUsers}/>
                        <Statdisplay statname="Avg. Read Time" value={this.state.avgReadTime / 60} unit="Minutes"/>
                    </div>
                   
                    <div className="cardscontainer">
                            <h3>Traffic Classifated</h3>
                            <p>Organic : { this.state.classify_traffic.organic } - { Math.ceil (this.state.classify_traffic.organic * 100 / this.state.classify_traffic.total) }%</p>
                            <p>Social : { this.state.classify_traffic.social } -  { Math.ceil (this.state.classify_traffic.social * 100 / this.state.classify_traffic.total ) }%</p>
                            <p>Direct : { this.state.classify_traffic.direct } -  { Math.ceil (this.state.classify_traffic.direct * 100 / this.state.classify_traffic.total ) }%</p>
                            <p>ref :  { this.state.classify_traffic.ref } - { Math.ceil (this.state.classify_traffic.ref * 100 / this.state.classify_traffic.total)  }%</p> 
                    </div>
                   
                    <div className="cardscontainer">
                        <StatListDisplay 
                            datalist={this.state.topUsersByVisits} 
                            fields={[{field:'_id._id'},{field:'_id.ip'},{field:'visitcount'}]} 
                            statname="Top Users" 
                        /> 
                        <StatListDisplay 
                            datalist={this.state.topPagesByVisits} 
                            fields={[{field:'_id.title'},{field:'visitcount'}]} 
                            statname="Top Pages By Visits" 
                        /> 
                        <StatListDisplay 
                            datalist={this.state.topPagesByReadtime} 
                            fields={[{field:'title',maxLength : 140}]} 
                            statname="Top Pages by ReadTime" 
                        />  
                    </div>
                    <div className="cardscontainer"> 
                        <StatListDisplay 
                            datalist={this.state.topMedias.topVideos} 
                            fields={[{field:'visitId'},{field:'title'},{field:'value',unit:'Minutes'}]} 
                            statname="Top Videos" 
                        />  
                        <StatListDisplay 
                            datalist={this.state.topMedias.topAudios} 
                            fields={[{field:'visitId'},{field:'title'},{field:'value',unit:'Minutes'}]} 
                            statname="Top Audios" 
                        />   
                    </div>  
               
                    <div className="cardscontainer"> 
                        <Statdisplay statname="bounce Rate" value={Math.ceil(this.state.bounceRate.bounceRate)} unit="%"/>
                        <StatListDisplay 
                            datalist={this.state.topPosts} 
                            fields={[{field:'_id.title'},{field:'visitcount'}]} 
                            statname="Top Posts" 
                        />    
                
                    </div>
                    <div className="cardscontainer">
                        <Statdisplay statname="New Users" value={this.state.numberNewUsers}/>
                        <Statdisplay statname="Returning Users" value={this.state.numberReturningUsers}/>
                        <Statdisplay statname="Loyal Users" value={this.state.numberLoyalUsers}/>
                    </div>
                    <div className="cardscontainer">
                        <StatListDisplay 
                            datalist={this.state.topAuthors} 
                            fields={[{field:'authors'},{field:'visitcount'}]} 
                            statname="Top Authors" 
                        />   
                        <div className="statcard">
                            <h3>Reading Time Par Palier</h3> 
                            <div>
                                <p>15 s : {this.state.readingTimeSections.a15}%</p>
                                <p>30 s : {this.state.readingTimeSections.a30}%</p>
                                <p>45 s : {this.state.readingTimeSections.a45}%</p>
                                <p>60 s : {this.state.readingTimeSections.a60}%</p>
                                <p>90 s : {this.state.readingTimeSections.a90}%</p> 
                            </div>
                        </div> 
                        <div className="statcard">
                            <h3>Scroll Palier des Utilisateurs</h3> 
                            <p>Jusqu'à 25% : { this.state.scrollPalier.users.a25}%</p>
                            <p>Jusqu'à 50% : { this.state.scrollPalier.users.a50}%</p>
                            <p>Jusqu'à 75% : { this.state.scrollPalier.users.a75}%</p>
                            <p>Jusqu'à 90% : { this.state.scrollPalier.users.a90}%</p> 
                        </div> 
                    </div>
                    <div className="cardscontainer"> 
                    <StatListDisplay 
                        datalist={this.state.scrollPalier.pages} 
                        fields={[{field:'title'},{field:'avgScroll',unit:'%'}]} 
                        statname="Top pages by Scroll" 
                    />   
                    <StatListDisplay 
                        datalist={this.state.bounceRate.topPages} 
                        fields={[{field:'_id'},{field:'boucerate',unit:'%'}]} 
                        statname="Top pages by BounceRate" 
                    />  
                    </div> 
                    <div className="cardscontainer">
                    <StatListDisplay 
                        datalist={this.state.usersByCountry} 
                        fields={[{field:'_id'},{field:'visitcount'}]} 
                        statname="Top Countries" 
                    />  
                    <StatListDisplay 
                        datalist={this.state.usersByRef} 
                        fields={[{field:'url'},{field:'refCount'}]} 
                        statname="Top Ref Websites" 
                    />  
                    <StatListDisplay 
                        datalist={this.state.usersByDevice} 
                        fields={[{field:'_id'},{field:'visitcount'}]} 
                        statname="Top Users Devices" 
                    />   
                </div>
                </div> 
            }
                
            </div>   
        )         
    } 
}

export default Stats