import React from 'react';
import './Statdisplay.css'

function Statdisplay (props) {
 
    return ( 
        <div className="statcard">
            <h3>{props.statname}</h3>
             <p>{props.value} {props.unit??''}</p> 
        </div> 
    )
  
}


export default Statdisplay