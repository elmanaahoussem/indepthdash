import React from 'react';
import './StatListDisplay.css'

function renderField(elm,f,key)
{
    let r 
    
    if (f.field.indexOf('.') === -1) { 
        if (elm[f.unit] === "Minutes")
            elm[f.field] /= 60
        if (elm[f.maxLength])
            elm[f.field] = elm[f.field].substr(0,elm[f.maxLength])

        return (<p key={key} >{elm[f.field]}{elm[f.unit]??''}</p>)
    } 

    f.field.split('.').map((e,i) => {return i === 0 ? r = elm[e] : r = r[e]} )
    
    if (elm[f.unit] === "Minutes")
        r /= 60
    if (elm[f.maxLength])
        r = r.substr(0,elm[f.maxLength])

    return  (<p key={key}>{r} {elm[f.unit]??''}</p>)
}

function StatListDisplay (props) { 
    return ( 
        <div className="statcard">
            <h3>{props.statname}</h3>
            { props.datalist.map( (e, key)=> {  
                return (
                <div key={key}> 
                    { props.fields.map( (f,key) => { 
                       return renderField(e,f,key) 
                       })
                    }  
                </div>
                ) 
            })
            } 
        </div>  
    ) 
}


export default StatListDisplay