import React , {Component} from 'react';
import './App.css';
import MaterialUIPickers from './MaterialUIPickers/MaterialUIPickers'
import {Tags} from './MultipleSelect/MultipleSelect'
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';
import Stats from './Stats/Stats'

const endPoint =  'http://localhost:3000/'//'https://plesk.indepth.inku.be/'

class App extends Component {
  
  constructor(props) {    
    super(props);  
    this.state = {
      startDate : new Date('2020-08-01'),
      endDate : new Date(),
      startPublishDate : new Date('2013-07-01'),
      endPublishDate :new Date(),
      numberofItems : 10,
      urls : [],
      pages : [],
      checkBoxPublishDate : false,
      banInkyteam: false
    }
    this.urls = []
  }

  componentDidMount() {
    axios.get( endPoint + 'stats/getpagelist' )
    .then(response => {
      this.setState({pages : response.data }) 
    })
    .catch(error => {
      console.log(error);
    });
  }

  multiplePagesChangeHandler(values) {
    this.setState({ urls: values }) 
  }

  publishDatesChangeHandler(startPublishDate,endPublishDate) {
    this.setState({
      startPublishDate : startPublishDate,
      endPublishDate : endPublishDate,
    })
  }

  datesChangeHandler(startDate,endDate) {
    this.setState({
      startDate : startDate,
      endDate : endDate,
    })
  }
   
  handleBanInkyteam (event)  {
    this.setState({ 
      banInkyteam : event.target.checked  
    });
  };

  handleChange (event)  {
    this.setState({ 
      checkBoxPublishDate : event.target.checked  
    });
  };

  handlerNumberOfposts (event) {
    this.setState ({
      numberofItems : event.target.value
    })
  }



  render () {
    return (
      <div className="App">
        <h2>Indepth</h2>
        <div className="select-container">
          <p>Visit Dates</p>
          <MaterialUIPickers startDate={new Date('2020-07-01')} endDate={new Date()} onChange={ this.datesChangeHandler.bind(this) } />
        </div> 

        <div className="select-container mb-5">
          <p>Inclure les Dates de publication dans ces dates</p>
          <Checkbox
            checked={this.state.checkBoxPublishDate}
            onChange={this.handleChange.bind(this)}
            inputProps={{ 'aria-label': 'primary checkbox' }}
          />
        </div>

        <div className="select-container mb-5">
          <p>Exclure Inkyfada IP</p>
          <Checkbox
            checked={this.state.banInkyteam}
            onChange={this.handleBanInkyteam.bind(this)}
            inputProps={{ 'aria-label': 'primary checkbox' }}
          />
        </div>
 
        <div className="select-container mb-5">
            <p>Number of Items to get In Top lists</p>
            <input type="text"  value={this.state.numberofItems} onChange={this.handlerNumberOfposts.bind(this)}/> 
        </div>
 
        <div className="select-container d-none">
          <p>Publish Article Dates</p>
          <MaterialUIPickers 
            startDate={new Date('2013-07-01')} 
            endDate={new Date()} 
            onChange={ this.publishDatesChangeHandler.bind(this) } 
          />
        </div> 
        <div className="select-container">
          <p>Pages List</p>
          <Tags urls={this.state.pages} onChange={this.multiplePagesChangeHandler.bind(this)} /> 
        </div>  
        <div className="stats-container">
          <Stats 
            endPoint={endPoint}
            startDate={this.state.startDate} 
            endDate={this.state.endDate} 
            startPublishDate={ this.state.checkBoxPublishDate ? this.state.startDate : this.state.startPublishDate} 
            endPublishDate={ this.state.checkBoxPublishDate ? this.state.endDate : this.state.endPublishDate }
            numberofItems = {this.state.numberofItems}
            urls={this.state.urls} 
            banInkyteam = {this.state.banInkyteam}
          /> 
        </div>
      </div>
    );
  }
}

export default App;
