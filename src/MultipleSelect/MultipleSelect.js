import React from 'react'; 
import { makeStyles  } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox'; 
import Autocomplete from '@material-ui/lab/Autocomplete'; 
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 1300,
    maxWidth: 1300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
  root: { 
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

export  function MultipleSelect(props) { 

  const classes = useStyles(); 
  const [personName, setPersonName] = React.useState([]);

  const handleChange = (event) => {
    setPersonName(event.target.value);  
    props.onChange(event.target.value)
  };  

  const urls = props.urls
 
  return (
    <div> 
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-mutiple-checkbox-label">Pages List</InputLabel>
        <Select
          labelId="demo-mutiple-checkbox-label"
          id="demo-mutiple-checkbox"
          multiple
          value={personName}
          onChange={handleChange}
          input={<Input />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MenuProps}
        >
          {urls.map((name) => (
            <MenuItem key={name} value={name}>
              <Checkbox checked={personName.indexOf(name) > -1} />
              <ListItemText primary={name} />
            </MenuItem>
          ))}
        </Select>
      </FormControl> 
    </div>
  );
}
 

export function Tags (props) {
  const urls =  props.urls.map((e) => { return {url:e} })
  const classes = useStyles();
  //const [personName, setPersonName] = React.useState([]);
  const handleChange = (params) => {
   // setPersonName(event.target.value); 
    console.log("éeezé",params.map((e) => e.url))
    props.onChange(params.map((e) => e.url))
  };  
  return (
    <div className={classes.root}>
      <Autocomplete
        multiple
        id="tags-standard"
        options={urls}  
        onChange={(event, value) => handleChange(value)  }
        getOptionLabel={(option) => option.url} 
        renderInput={(params) => { 
          return (
          <TextField
            {...params}
            variant="standard"
            label="Multiple values"
            placeholder="Favorites"
          />
        )}}
      />
    </div>)
}
